const userModel = require("../models/userModel");

// khởi tạo thư viện mongoose
const mongoose = require("mongoose");

const createUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let requestBody = request.body;
    //B2: Validate dữ liệu
    if (!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is invalid"
        })
    }
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "username is invalid"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "email is invalid"
        })
    }
    if (!requestBody.address) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "address is invalid"
        })
    }
    if (!requestBody.phone) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "address is invalid"
        })
    }
    if (!requestBody.website) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "website is invalid"
        })
    }
    if (!requestBody.company) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "company is invalid"
        })
    }

    let userInput = {
        _id: mongoose.Types.ObjectId(),
        name: requestBody.name,
        username: requestBody.username,
        email: requestBody.email,
        address: {
            street: requestBody.address.street,
            suite: requestBody.address.suite,
            city: requestBody.address.city,
            zipcode: requestBody.address.zipcode,
            geo: {
                lat: requestBody.address.geo.lat,
                lng: requestBody.address.geo.lng,
            }
        },
        phone: requestBody.phone,
        website: requestBody.website,
        company: {
            name: requestBody.company.name,
            catchPhrase: requestBody.company.catchPhrase,
            bs: requestBody.company.bs
        }
    }

    userModel.create(userInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Create Review Success",
                data: data
            })
        }
    })
};

const getAllUser = (request, response) => {
    userModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get courses success",
                data: data
            })
        }
    })
};

const updateUserById = (request, response) => {
    let userId = request.params.userId;
   let requestBody = request.body;

   if (!mongoose.Types.ObjectId.isValid(userId)) {
      return response.status(400).json({
         status: "Error 400: Bad Request",
         message: "User ID is not valid"
      })
   }

   let newUpdateUser = {
      name: requestBody.name,
      username: requestBody.username,
      email: requestBody.email,
      address: {
         street: requestBody.address.street,
         suite: requestBody.address.suite,
         city: requestBody.address.city,
         zipcode: requestBody.address.zipcode,
         geo: {
            lat: requestBody.address.geo.lat,
            lng: requestBody.address.geo.lng,
         }
      },
      phone: requestBody.phone,
      website: requestBody.website,
      company: {
         name: requestBody.company.name,
         catchPhrase: requestBody.company.catchPhrase,
         bs: requestBody.company.bs
      }
   }

   userModel.findByIdAndUpdate(userId, newUpdateUser, (error, data) => {
      if (error) {
         return response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
         })
      } else {
         return response.status(201).json({
            status: "Success: User updated",
            //data: data
         })
      }
   })
};

const getUserById = (request, response) => {
    let userId = request.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
       return response.status(400).json({
          status: "Error 400: Bad Request",
          message: "User ID is not valid"
       })
    }
 
    userModel.findById(userId, (error, data) => {
       if (error) {
          return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
          })
       } else {
          return response.status(200).json({
             status: "Success: Get User success",
             data: data
          })
       }
    })
};

const deleteUser = (request, response) => {
    let userId = request.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
       return response.status(400).json({
          status: "Error 400: Bad Request",
          message: "User ID is not valid"
       })
    }
 
    userModel.findByIdAndDelete(userId,(error,data)=>{
       if(error) {
          return ResizeObserver.status(500).json({
              status: "Error 500: Internal server error",
              message: error.message
          })
      } else {
          return response.status(204).json({
              status: "Success: Delete User success"
          })
      }
    })
};

module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    updateUserById: updateUserById,
    getUserById: getUserById,
    deleteUser: deleteUser
}