//khai báo thư viện 
const express = require("express");

const userRouter = express.Router();

const {createUser, getAllUser, updateUserById, getUserById, deleteUser} = require("../controller/userController");

//create a user
userRouter.post("/users", createUser);
userRouter.get("/users", getAllUser);
userRouter.put("/users/:userId", updateUserById);
userRouter.get("/users/:userId", getUserById);
userRouter.delete("/users/:userId", deleteUser);
module.exports = userRouter;