const postModel = require("../models/postModel");
const userModel = require("../models/userModel");
const mongoose = require("mongoose");

const createPost = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is invalid"
        })
    }
    
    if (!bodyRequest.title) {
        return response.status(400).json({
          status: "Error 400: Bad Request",
          message: "title is required"
        })
      }

      if (!bodyRequest.body) {
        return response.status(400).json({
          status: "Error 400: Bad Request",
          message: "body is required"
        })
      }
    //B3: Thao tác với cơ sở dữ liệu
    let postInput = {
        _id: mongoose.Types.ObjectId(),
        title: requestBody.title,
        body: requestBody.body
    }

    postModel.create(postInput, (error, data) => {
        if (error) {
            return response.status(500).json({
              status: "Error 500: Internal server error",
              message: error.message
            })
          } else {
            response.status(200).json({
              status: "Success: Create post success",
              data: data
            })
        }
    })
}

const getAllPost = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    postModel.find((error, data) => {
        if (error) {
          response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
          })
        } else {
          response.status(200).json({
            status: "Success: Get all post success",
            data: data
          })
        }
      })
}

const getPostById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    postModel.findById(postId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get post by ID success",
                data: data
            })
        }
    })
}

const getPostsOfUser=(request,response)=>{
        //B1: Chuẩn bị dữ liệu
        let userId = request.params.courseId;
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(userId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        }
        //B3: Thao tác với cơ sở dữ liệu
        postModel.find(userId,(error, data) => {
            if (error) {
              response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
              })
            } else {
              response.status(200).json({
                status: "Success: Get post of user success",
                data: data
              })
            }
          })
}
const updatePostById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let postUpdate = {
        title: bodyRequest.title,
        body: bodyRequest.body
    }

    postModel.findByIdAndUpdate(postId, postUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update post success",
                data: data
            })
        }
    })
}

const deletePostById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //let historyId = request.params.diceId;
  let postId = request.params.postId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(postId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Post ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  postModel.findByIdAndDelete(postId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(204).json({
            status: "Success: Delete Post success"
            })
        }
    })
}

module.exports = {
    createPost: createPost,
    getAllPost: getAllPost,
    getPostById: getPostById,
    updatePostById: updatePostById,
    deletePostById: deletePostById,
    getPostsOfUser:getPostsOfUser
}
