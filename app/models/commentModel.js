//B1: khai báo sử dụng mongoose
const mongoose = require("mongoose");

//B2: khai báo schema
const Schema = mongoose.Schema;

//B3: khơi tạo 1 schema
const commentSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    name : {
        type : String,
        required : true
    },
    email : {
            type : String,
        required : true    
    },
    body : {
        type : String,
        required : true
    },
    postId : {
        type : mongoose.Types.ObjectId,
        ref : "post"
    }
});

//B4: export model compile từ schema
module.exports = mongoose.model("comment", commentSchema);