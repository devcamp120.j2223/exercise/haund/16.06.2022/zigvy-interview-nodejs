const express = require('express');
const router = express.Router();

//Import các hàm trong postController
const {createPost, getAllPost, getPostById, getPostsOfUser, updatePostById, deletePostById} = require("../controller/postController");


router.post('/posts', createPost);

router.get('/posts', getAllPost);

router.get('/posts/:postId', getPostById);

router.get('/users/:userId/posts', getPostsOfUser);

router.put('/posts/:postId', updatePostById);

router.delete('/posts/:postId', deletePostById);

module.exports = router;
